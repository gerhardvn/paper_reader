require 'serrano'

# remove white space and merge multi line references
# create a single line for a reference.
def merge_lines(lines)
  merged_lines = []
  current_line = ""

  lines.each do |line|
    if line.match(/^\d+\./) # Check if line starts with a number followed by a dot
      merged_lines << current_line unless current_line.empty?
      current_line = line.squeeze(' ')
    else
      current_line += " " + line.squeeze(' ')
    end
  end

  merged_lines << current_line unless current_line.empty?
  merged_lines
end

# Take reference and do a online check for an ID
def find_doi(reference)
  results = Serrano.works(query: reference)
  # lookup worked
  if results["message"] && results["message"]["items"]
    item = results["message"]["items"].first
    if item["DOI"]
      return item["DOI"]
    end
  end
  # lookup failed
  return nil
end

# Use ID to het metadata
def fetch_metadata(doi)
  response = Serrano.works(ids: doi)[0]
  # lookup ok 
  if response 
    return response['message']
  end
  # Lookup failed
  nil
  
 end

# create an APA reference from a ID
def doi_to_apa(doi)
  data = fetch_metadata(doi)
  if data then
    title = data['title']
    authors = data['author'].map { |author| "#{author['given']} #{author['family']}" }
    year = data['created']['date-parts'][0][0]
    # Construct the APA reference
    return "#{authors.join(', ')}. (#{year}). #{title}."
  end
  # missing meta data
  rescue
    nil
end

#
def remove_until_first_space(string)
  string.sub(/\A\S+\s/, '')
end

# compute a similarity score for two references.
# Jaccard similarity
def similarity_score(string1, string2)
  set1 = string1.downcase.split('').to_set
  set2 = string2.downcase.split('').to_set

  intersection = set1.intersection(set2)
  union = set1.union(set2)

  intersection.size.to_f / union.size.to_f
end


def parse_reference_list(text)
  merged_lines = merge_lines(text)
  merged_lines.each do |line|
    line = remove_until_first_space(line)
    puts line
    puts "---"
    doi = find_doi(line)
    if doi
      puts "DOI: #{doi}"
      puts "---"
      apa_reference = doi_to_apa(doi)
      if apa_reference
        puts apa_reference
        puts "---"
        score = similarity_score(line, apa_reference)
        puts "Similarity score: #{score}"
      else
        puts "Unable to convert DOI to APA reference."
      end
    else
      puts "DOI not found for the given reference."
    end
    puts "----------------------------------------------------------"
  end
end

