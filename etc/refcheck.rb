require 'nokogiri'
require 'open-uri'

def reference_exists?(reference)
  search_url = "https://scholar.google.com/scholar?q=#{URI.encode_www_form_component(reference)}"
  
  doc = Nokogiri::HTML(URI.open(search_url))
  results = doc.css('.gs_ri') # CSS selector for search results
  
  results.each do |result|
    title = result.css('.gs_rt a').text
    if title.include?(reference)
      return true
    end
  end
  
  false
end

# Example usage:
reference = "Your Reference Here"
if reference_exists?(reference)
  puts "The reference exists on Google Scholar."
else
  puts "The reference does not exist on Google Scholar."
end
