filename = ARGV[0]

if filename.nil?
  puts "No file name provided. Please provide a file name as a command-line argument."
  exit
else
  if !File.exist?(filename)
    puts "File '#{filename}' does not exist."
	exit
  end
end

def merge_lines(filename)
  merged_lines = []
  current_line = ""

  File.foreach(filename) do |line|
    if line.match(/^\d+\./) # Check if line starts with a number followed by a dot
      merged_lines << current_line unless current_line.empty?
      current_line = line.chomp
    else
      current_line += " " + line.chomp
    end
  end

  merged_lines << current_line unless current_line.empty?
  merged_lines
end

require 'serrano'

def find_doi(reference)
  results = Serrano.works(query: reference)
  if results["message"] && results["message"]["items"]
    item = results["message"]["items"].first
    if item["DOI"]
      return item["DOI"]
    end
  end
  return nil
end

def fetch_metadata(doi)
  response = Serrano.works(ids: doi)[0]
 
  if response 
    return response['message']
  end
  
  nil
  
 end

def doi_to_apa(doi)
  data = fetch_metadata(doi)
  if data then
    title = data['title']
    authors = data['author'].map { |author| "#{author['given']} #{author['family']}" }
    year = data['created']['date-parts'][0][0]
    # Construct the APA reference
    return "#{authors.join(', ')}. (#{year}). #{title}."
  end
  nil
end

def remove_until_first_space(string)
  string.sub(/\A\S+\s/, '')
end

def similarity_score(string1, string2)
  set1 = string1.downcase.split('').to_set
  set2 = string2.downcase.split('').to_set

  intersection = set1.intersection(set2)
  union = set1.union(set2)

  intersection.size.to_f / union.size.to_f
end


merged_lines = merge_lines(filename)
merged_lines.each do |line|
  line = remove_until_first_space(line)
    puts line
  puts "---"
  doi = find_doi(line)
  if doi
    puts "DOI: #{doi}"
	puts "---"
	apa_reference = doi_to_apa(doi)
    if apa_reference
      puts apa_reference
      puts "---"
	  
	  score = similarity_score(line, apa_reference)
      puts "Similarity score: #{score}"

    else
      puts "Unable to convert DOI to APA reference."
    end
  else
    puts "DOI not found for the given reference."
  end
  puts "----------------------------------------------------------"
  
end

