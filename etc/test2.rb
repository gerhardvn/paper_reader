require 'serrano'
require 'citeproc'
require 'csl/styles'

cp = CiteProc::Processor.new style: 'apa', format: 'text'

cp.import Serrano.content_negotiation(ids: '10.1126/science.169.3946.635', format: "citeproc-json")
cp.render :bibliography