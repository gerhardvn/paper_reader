require 'pdf/reader'

# commandline argument ius pdf file to parse
pdf_file_path = ARGV[0]

if pdf_file_path.nil?
  puts "No file name provided. Please provide a file name as a command-line argument."
  exit
elsif !File.exist?(pdf_file_path)
  puts "File '#{pdf_file_path}' does not exist."
	exit
end

reader = PDF::Reader.new(pdf_file_path)

reader.pages.each do |page|
  print page.inspect
  puts "------------------------------------------------------------------------------------------------------"
  puts page.text
end




