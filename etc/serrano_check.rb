filename = ARGV[0]

if filename.nil?
  puts "No file name provided. Please provide a file name as a command-line argument."
  exit
else
  if !File.exist?(filename)
    puts "File '#{filename}' does not exist."
	exit
  end
end

def merge_lines(filename)
  merged_lines = []
  current_line = ""

  File.foreach(filename) do |line|
    if line.match(/^\d+\./) # Check if line starts with a number followed by a dot
      merged_lines << current_line unless current_line.empty?
      current_line = line.chomp
    else
      current_line += " " + line.chomp
    end
  end

  merged_lines << current_line unless current_line.empty?
  merged_lines
end

require 'serrano'

def find_doi(reference)
  results = Serrano.works(query: reference)
  if results["message"] && results["message"]["items"]
    item = results["message"]["items"].first
    if item["DOI"]
      return item["DOI"]
    end
  end
  return nil
end


merged_lines = merge_lines(filename)
merged_lines.each do |line|
  puts line
  puts "---"
  doi = find_doi(line)
  if doi
    puts "DOI: #{doi}"
  else
    puts "DOI not found for the given reference."
  end
  puts "----------------------------------------------------------"
  
end

