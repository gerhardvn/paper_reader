filename = ARGV[0]

if filename.nil?
  puts "No file name provided. Please provide a file name as a command-line argument."
  exit
else
  if !File.exist?(filename)
    puts "File '#{filename}' does not exist."
	exit
  end
end

def merge_lines(filename)
  merged_lines = []
  current_line = ""

  File.foreach(filename) do |line|
    if line.match(/^\d+\./) # Check if line starts with a number followed by a dot
      merged_lines << current_line unless current_line.empty?
      current_line = line.chomp
    else
      current_line += " " + line.chomp
    end
  end

  merged_lines << current_line unless current_line.empty?
  merged_lines
end

require 'nokogiri'
require 'open-uri'
require 'timeout'

def fetch_html(url)
  begin
    html = URI.open(url, 'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 13_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15')
    doc = Nokogiri::HTML(html)
    return doc
  rescue OpenURI::HTTPError => e
    if e.io.status[0] == '429'
      puts "Server Response (429 Too Many Requests):"
      puts e.io.read
    else
      puts "Error: #{e.message}"
    end
    return nil
  rescue => e
    puts "Error: #{e.message}"
    return nil
  end
end

def reference_exists?(reference)
  search_url = "https://scholar.google.com/scholar?q=#{URI.encode_www_form_component(reference)}"
  
  doc = fetch_html(search_url)
  
#  x = URI.open(search_url, 'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 13_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15')
#  doc = Nokogiri::HTML(x)

  if doc
    results = doc.css('.gs_ri') # CSS selector for search results
 
    results.each do |result|
      title = result.css('.gs_rt a').text
      if title.include?(reference)
        return true
      end
    end
  end
  
  false
end

merged_lines = merge_lines(filename)
merged_lines.each do |line|
  puts line
  puts "---"
  if reference_exists?(line) then
    puts "The reference exists on Google Scholar."
  else
    puts "The reference does not exist on Google Scholar."
	puts "---"
	puts "https://scholar.google.com/scholar?q=#{URI.encode_www_form_component(line)}"
  end	
  puts "----------------------------------------------------------"
  
end
