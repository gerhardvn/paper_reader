require 'serrano'

def find_doi(reference)
  results = Serrano.works(query: reference)
  if results["message"] && results["message"]["items"]
    item = results["message"]["items"].first
    if item["DOI"]
      return item["DOI"]
    end
  end
  return nil
end

# Example usage:
reference = "Wilcox MJ, Guimond A, Campbell PH, Weintraub Moore H. Provider Perspectives on the Use of Assistive Technology for Infants and Toddlers With Disabilities. Topics Early Child Spec Educ. 2006;26(1):33–49." # Replace with the actual reference

doi = find_doi(reference)
if doi
  puts "DOI: #{doi}"
else
  puts "DOI not found for the given reference."
end