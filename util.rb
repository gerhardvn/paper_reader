def find_reference_start(reader)
  reference_found = 0
  reader.pages.each do |page|
    print '.'
    page_lines = exclude_headers (page.text)
    page_lines.each do |line|
      if (line.downcase.include? 'reference')
        reference_found = page.number - 1
      end
    end 
  end  
  puts ""
  reference_found
end

def parse_document(reader, reference_list_page )
  text = ''
  reader.pages.each_with_index do |page, i|
    print '.'
    page_lines = exclude_headers (page.text)
    if (i == reference_list_page) then
      reference_line_found = false
      new_lines = ''
      page_lines.each do |line|
        if reference_line_found
          new_lines += line
        end
        if (line.downcase.include? 'reference')  
          reference_line_found = true
        end
      end
      text += new_lines
    elsif (i > reference_list_page) then
      text += page_lines.join
    else
      line_ref_check(page_lines)    
    end
  end  
  puts ""
  text
end


# return only text that has more lowercase than upper case letters
def remove_lines_with_more_uppercase(text)
  lines_with_less_uppercase = []
  
  text.each do |line|
    uppercase_count = line.count("A-Z")
    lowercase_count = line.count("a-z")
    
    if uppercase_count < lowercase_count or
       uppercase_count == 0
      lines_with_less_uppercase << line
    end
  end
  
  lines_with_less_uppercase
end


def exclude_headers(text)
  # Customize this method based on your PDF's header characteristics
  # For example, you can check the position, font size, or keywords
  
  text_lines = remove_lines_with_more_uppercase(text.lines)
  text_lines
end