require 'pdf/reader'
require './reference_counter.rb'
require './reference_check.rb'
require './util.rb'

# Usage
if ARGC != 2
  puts "Usage pdf_paper_read.rb [reference] [pdf_file]"
  puts "Ex: pdf_paper_read.rb 0 test.pdf"
  exit
end

# reference style
begin 
  reference_style = Integer(ARGV[1])
rescue
  puts "Reference must be a integer"
  exit
end

# commandline argument is pdf file to parse
pdf_file_path = ARGV[1]

if pdf_file_path.nil?
  puts "No file name provided. Please provide a file name as a command-line argument."
  exit
elsif !File.exist?(pdf_file_path)
  puts "File '#{pdf_file_path}' does not exist."
	exit
end



reader = PDF::Reader.new(pdf_file_path)

reference_list_page = find_reference_start(reader)
reference_list = parse_document(reader, reference_list_page)

output_ref_counts()

parse_reference_list(reference_list.split("\n"))



