# string containing al references found in text
# array of numbers seperated by comma
$ref_list = ""

# ------------------------------------------------------------------------
def line_ref_check(line)

  fail unless line.kind_of?(Array)

  line.each do |line|
    line.scan(/^\s+(\d{1,3})$/) do |match|
      $ref_list += match[0]
      $ref_list += ','
    end
    line.scan(/^\s+(\d{1,3},\d{1,3})$/) do |match|
      $ref_list += match[0]
      $ref_list += ','
    end
    line.scan(/((\d+,)((\d+|.),){1,3}(\d+))/) do |match|
      puts match[0]
    end
  end

  # Character translations  
  line = line.to_s.gsub('–','-')

  # scan for (1) or (2,3)
  ref = line.scan(/\(([0-9,]+)\)/)
  if ((!ref.nil?) and (ref.length > 0))
    ref.each do |x|
      $ref_list += x[0]
      $ref_list += ','
    end
  end
  
  # scan for (42,p123)
  ref = line.scan(/\(([0-9]+),p.[0-9]+\)/)
  if ((!ref.nil?) and (ref.length > 0))
    ref.each do |x|
      $ref_list += x[0]
      $ref_list += ','
    end
  end
  
  # scan for (1-5)
  ref = line.scan(/\(([0-9]+[-]+[0-9]+)\)/)
  if ((!ref.nil?) and (ref.length > 0))
    ref.each do |x|
      start_num, end_num = x[0].split("-").map(&:to_i)
      numbers_list = (start_num..end_num).to_a
      $ref_list += numbers_list.join(",") + ","
    end
  end
end

# ------------------------------------------------------------------------
def output_ref_counts
  if $ref_list != ''
    # Split the string into an array of numbers
    numbers_array = $ref_list.split(",")
    # Convert the array elements to integers
    numbers_array.map!(&:to_i)
    # Remove duplicates and sort the array
    unique_numbers = numbers_array.uniq.sort
    # Print the resulting array
    puts unique_numbers.inspect
    # Create array with all numbers
    full_range = (1..unique_numbers.last).to_a
    # Compute the missing numbers
    missing_numbers = full_range - numbers_array
    # Print the missing numbers
    puts missing_numbers.inspect
  else
    puts "reference counting failed"
  end
end
